module.exports = {
  resources: {
    maxAge: 0
  },
  logging: {
    xResponseTime: true,
    requestsTime: true
  },
  server: {
    port: 3000
  }
}
